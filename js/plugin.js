// Models
let items = JSON.parse(localStorage.getItem('items')) || [];

// Variables
let form = document.forms['add-item'],
    inputName = document.getElementById('itemName'),
    inputPrice = document.getElementById('itemPrice'),
    btnAddItem = document.querySelector('form button'),
    btnSortItem = document.querySelector('.sort'),
    table = document.querySelector('.table tbody'),
    alertDiv = table.parentElement,
    minPrice = document.getElementById('minPrice'),
    maxPrice = document.getElementById('maxPrice'),
    sort = 'asc';

// random ID
function generateId() {
    let id = '';
    let words = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';

    for (let i = 0; i < 15; i++) {
        let position = Math.floor(Math.random() * words.length);
        id += words[position];
    }

    return id;
}

// clear list
function clearCard() {
    table.innerHTML = '';
}

// show message
function showAlert(settings) {
    let div = document.createElement('div');

    div.className = 'alert mb-4 ' + settings.cssClass;
    div.innerHTML = settings.text;

    alertDiv.parentElement.insertBefore(div, alertDiv);

    if (settings.timeout !== undefined) setTimeout(() => div.remove(), settings.timeout);
}


// create list with items
function cardTemplate(item) {
    let tr = document.createElement('tr');
    tr.setAttribute('data-id', item.id);

    let item_name = document.createElement('td');
    item_name.innerHTML = '<span class="name">' + item.name + '</span>';

    let item_price = document.createElement('td');
    item_price.innerHTML = '<span class="price">' + item.price + '</span>';

    // Create tag i fa-edit
    let iEdit = document.createElement('td');
    iEdit.innerHTML = '<i class="fas fa-edit edit-item"></i>';

    // Create tag i fa-trash
    let iDelete = document.createElement('td');
    iDelete.innerHTML = '<i class="fas fa-trash delete-item"></i>';

    tr.appendChild(item_name);
    tr.appendChild(item_price);
    tr.appendChild(iEdit);
    tr.appendChild(iDelete);

    return tr;
}

function generateCardList(itemsArray) {
    checkEmptyList(itemsArray);

    clearCard();

    for (let i = 0; i < itemsArray.length; i++) {
        let tr = cardTemplate(itemsArray[i]);
        table.appendChild(tr);
    }
}

// show message if list is empty
function checkEmptyList(arr) {
    if (arr.length === 0) showAlert({
        cssClass: 'alert-info',
        text: 'Empty list'
    });
}

// add item function
function addList(name, price) {
    let newTask = {
        id: generateId(),
        name: name,
        price: price
    };

    items.unshift(newTask);

    table.insertAdjacentElement('afterbegin', cardTemplate(newTask));

    showAlert({
        cssClass: 'alert-success',
        timeout: 3000,
        text: 'Item added success!'
    });

    localStorage.setItem('items', JSON.stringify(items));

    form.reset();
}

// Remove item
function deleteItem(trash) {
    let parent = trash.closest('tr'),
        id = parent.dataset.id;

    for (let i = 0; i < items.length; i++){
        if (id === items[i].id){
            items.splice(i, 1);
            break;
        }
    }

    showAlert({
        cssClass: 'alert-danger',
        timeout: 3000,
        text: 'Task has been removed success'
    });

    localStorage.setItem('items', JSON.stringify(items));
    parent.remove();

    checkEmptyList(items);
}

// Edit item
function editListItem(id, name, price) {
    let newItem = {
        id: id,
        name: name,
        price: price
    };

    for( let i = 0; i < items.length; i++){
        if (id === items[i].id){
            items.splice(i, 1, newItem);
            break
        }
    }

    localStorage.setItem('items', JSON.stringify(items));

    showAlert({
        cssClass: 'alert-info',
        timeout: 3000,
        text: 'Task updated success'
    });
}

// Sort item function
function sortItem(min, max) {
    let newItemsList = [];

    for (let i = 0; i < items.length; i++) {
        if (items[i].price < min || items[i].price > max) continue;

        newItemsList.unshift(items[i])
    }

    if (sort === 'asc') {
        sort = 'desc';

        newItemsList.sort(function (prev, next) {
            return prev.price - next.price
        });
    } else if (sort === 'desc') {
        sort = 'asc';
        newItemsList.sort(function (prev, next) {
            return next.price - prev.price
        });
    }

    generateCardList(newItemsList);
}

// Sort item
btnSortItem.addEventListener('click', function (e) {
    e.preventDefault();

    if (!minPrice.value) {
        minPrice.classList.add('is-invalid');
    } else {
        minPrice.classList.remove('is-invalid');
    }

    if (!maxPrice.value) {
        maxPrice.classList.add('is-invalid');
    } else {
        maxPrice.classList.remove('is-invalid');
    }

    if (minPrice.value && maxPrice.value) sortItem(minPrice.value, maxPrice.value);
});

// Add new item
btnAddItem.addEventListener('click', function (e) {
    e.preventDefault();

    if (!inputName.value) {
        inputName.classList.add('is-invalid');
    } else {
        inputName.classList.remove('is-invalid');
    }

    if (!inputPrice.value) {
        inputPrice.classList.add('is-invalid');
    } else {
        inputPrice.classList.remove('is-invalid');
    }

    if (inputName.value && inputPrice.value) addList(inputName.value, inputPrice.value);
});

// Delete and change item
table.addEventListener('click', function(e) {
    if (e.target.classList.contains('fa-trash')) {
        deleteItem(e.target);
    } else if (e.target.classList.contains('edit-item')) {
        let parent = e.target.closest('tr'),
            id = parent.dataset.id,
            name = parent.querySelector('.name'),
            price = parent.querySelector('.price');

        e.target.classList.toggle('fa-save');

        if (e.target.classList.contains('fa-save')) {
            name.setAttribute('contenteditable', 'true');
            price.setAttribute('contenteditable', 'true');
            name.focus();
        } else {
            name.setAttribute('contenteditable', 'false');
            price.setAttribute('contenteditable', 'false');
            name.blur();
            editListItem(id, name.textContent, price.textContent);
        }
    }
});

generateCardList(items);
